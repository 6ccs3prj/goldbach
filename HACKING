Goldbach Contribution Guidelines

Prerequirements
===============

    Make sure you have the following packages available prior to configure and
compile from the repository:

        * Git Version Control System
            http://git-scm.com/

        * CMake Cross Platform Make (2.8)
            http://www.cmake.org/

        * pkg-config helper tool
            http://pkg-config.freedesktop.org/

Requirements
============

    In order to configure and compile the source code the following dependencies
are needed:

        * F# compiler and tools
            http://www.microsoft.com/downloads/en/details.aspx?FamilyID=effc5bc4-c3df-4172-ad1c-bc62935861c5

        * Apache log4net
            http://logging.apache.org/log4net/

        * GNU gettext
            http://www.gnu.org/software/gettext/

        * Gtk# (3.0)
            https://github.com/mono/gtk-sharp

Coding Standards
================

    Follow the Code Formatting Guidelines (F#) for Visual Studio 2010, that can
be found at:

        http://msdn.microsoft.com/en-us/library/dd233191.aspx

Documentation
=============

    Follow the XML Documentation (F#) for Visual Studio 2010, that can be found
at:

        http://msdn.microsoft.com/en-us/library/dd233217.aspx

The FsHtmlDoc from the F# PowerPack tools is used to generate F#
Documentation. It can be obtained from:

        http://fsharppowerpack.codeplex.com/

To generate HTML documentation from the XMLDoc, perform the following command:

        make fsharpdoc

Internationalisation
====================

    To generate gettext po/pot files, perform the following command:

        make xgettext
