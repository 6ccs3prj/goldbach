# determine the compiler to use for F# programs

if(NOT CMAKE_FSharp_COMPILER)
    # prefer the environment variable FSC
    if($ENV{FSC} MATCHES ".+")
        if (EXISTS $ENV{FSC})
            message(STATUS "Found compiler set in environment variable FSC: $ENV{FSC}.")
            set(CMAKE_FSharp_COMPILER $ENV{FSC})
        else (EXISTS $ENV{FSC})
            message(SEND_ERROR "Could not find compiler set in environment variable FSC:\n$ENV{FSC}.")
        endif (EXISTS $ENV{FSC})
    endif($ENV{FSC} MATCHES ".+")

    # if no compiler has been specified yet, then look for one
    if (NOT CMAKE_FSharp_COMPILER)
        find_package(Mono)
        find_program(CMAKE_FSharp_COMPILER NAMES fsc)
        message(STATUS "Path of fsc: ${CMAKE_FSharp_COMPILER}")
        # set (CMAKE_FSharp_COMPILER "${FSC_EXECUTABLE}")

        # still not found, try fsc.exe
        if (NOT CMAKE_FSharp_COMPILER)
            get_filename_component(dotnet_path "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\.NETFramework;InstallRoot]" PATH)
            find_program(CMAKE_FSharp_COMPILER NAMES fsc PATHS "${dotnet_path}/Framework/v4.0.30319")
            file(TO_NATIVE_PATH "${dotnet_path}/Framework/v4.0.30319" native_path)
            message(STATUS "Looking for fsc: ${CMAKE_FSharp_COMPILER}")

            # give up
            if (NOT CMAKE_FSharp_COMPILER)
                message (STATUS "Couldn't find a valid F# compiler. Set either CMake_FSharp_COMPILER or the FSC environment variable to a valid path.")
            endif (NOT CMAKE_FSharp_COMPILER)
        endif (NOT CMAKE_FSharp_COMPILER)
    endif (NOT CMAKE_FSharp_COMPILER)
endif(NOT CMAKE_FSharp_COMPILER)

# now try to find the gac location
if (CMAKE_FSharp_COMPILER AND NOT GAC_DIR AND MONO_FOUND)
    find_package(PkgConfig)

    if (PKG_CONFIG_FOUND)
        pkg_search_module(MONO_CECIL mono-cecil)
        if(MONO_CECIL_FOUND)
            execute_process(COMMAND ${PKG_CONFIG_EXECUTABLE} mono-cecil --variable=assemblies_dir OUTPUT_VARIABLE GAC_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
        endif(MONO_CECIL_FOUND)

        pkg_search_module(CECIL cecil)
        if(CECIL_FOUND)
            execute_process(COMMAND ${PKG_CONFIG_EXECUTABLE} cecil --variable=assemblies_dir OUTPUT_VARIABLE GAC_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
        endif(CECIL_FOUND)

        if (NOT GAC_DIR)
            execute_process(COMMAND ${PKG_CONFIG_EXECUTABLE} mono --variable=libdir OUTPUT_VARIABLE MONO_LIB_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
            if (MONO_LIB_DIR)
                set (GAC_DIR "${MONO_LIB_DIR}/mono")
                message (STATUS "Could not find cecil, guessing GAC dir from mono prefix: ${GAC_DIR}")
            endif (MONO_LIB_DIR)
        endif (NOT GAC_DIR)
    endif (PKG_CONFIG_FOUND)

    if (NOT GAC_DIR)
        set (GAC_DIR "/usr/lib/mono")
        message(STATUS "Could not find cecil or mono. Using default GAC dir: ${GAC_DIR}")
    endif (NOT GAC_DIR)
endif (CMAKE_FSharp_COMPILER AND NOT GAC_DIR AND MONO_FOUND)

# Create a cache entry so the user can modify this.
set(GAC_DIR "${GAC_DIR}" CACHE PATH "Location of the GAC")
message(STATUS "Using GAC dir: ${GAC_DIR}")

mark_as_advanced(CMAKE_FSharp_COMPILER)

if (CMAKE_FSharp_COMPILER)
    set (CMAKE_FSharp_COMPILER_LOADED 1)
endif (CMAKE_FSharp_COMPILER)

# CMAKE_CURRENT_LIST_DIR is only supported in 2.8.3 and up
get_filename_component(script_dir ${CMAKE_CURRENT_LIST_FILE} PATH)

# configure variables set in this file for fast reload later on
configure_file(${script_dir}/CMakeFSharpCompiler.cmake.in
  ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeFSharpCompiler.cmake IMMEDIATE @ONLY)
set(CMAKE_FSharp_COMPILER_ENV_VAR "FSC")
