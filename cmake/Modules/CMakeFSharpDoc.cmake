if(UNIX)
    find_program(MONO_EXECUTABLE NAMES mono)
endif(UNIX)
find_program(FSHTMLDOC_EXECUTABLE NAMES fshtmldoc.exe FsHtmlDoc.exe PATHS /usr/lib/fsharp/ PATH_SUFFIXES v2.0 v4.0)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FSHTMLDOC DEFAULT_MSG FSHTMLDOC_EXECUTABLE)
if(UNIX)
    find_package_handle_standard_args(MONO DEFAULT_MSG MONO_EXECUTABLE)
endif(UNIX)

mark_as_advanced(FSHTMLDOC_EXECUTABLE)
if(UNIX)
    mark_as_advanced(MONO_EXECUTABLE)
endif(UNIX)

if(${FSHTMLDOC_FOUND})
    set(XML_FILES)
    foreach(dll ${TARGET_FS})
        list(APPEND XML_FILES ${CMAKE_BINARY_DIR}/${dll})
    endforeach(dll ${TARGET_FS})

    if(UNIX)
        add_custom_target("fsharpdoc"
        COMMAND
            ${MONO_EXECUTABLE}
            ${FSHTMLDOC_EXECUTABLE}
            ${XML_FILES}
        DEPENDS
            ${XML_FILES}
        WORKING_DIRECTORY
            ${CMAKE_BINARY_DIR}
        COMMENT
            "FsHtmlDoc ${TARGET_FS}"
        )
    else(UNIX)
        add_custom_target("fsharpdoc"
        COMMAND
            ${FSHTMLDOC_EXECUTABLE}
            ${XML_FILES}
        DEPENDS
            ${XML_FILES}
        WORKING_DIRECTORY
            ${CMAKE_BINARY_DIR}
        COMMENT
            "FsHtmlDoc ${TARGET_FS}"
        )
    endif(UNIX)
else(${FSHTMLDOC_FOUND})
    message(STATUS "Skipping F# documentation generation!")
endif(${FSHTMLDOC_FOUND})
