# This macro invokes msgfmt and generates the language resources. The
# following options are valid:
#
# SOURCE:  a list of source files
# OPTIONS: additional msgfmt options

include(ParseArguments)
find_program(MSGFMT_EXECUTABLE NAMES msgfmt)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MSGFMT DEFAULT_MSG MSGFMT_EXECUTABLE)

mark_as_advanced(MSGFMT_EXECUTABLE)

macro(gettext output)
    if(${MSGFMT_FOUND})
        parse_arguments(ARGS "SOURCE;OPTIONS" "" ${ARGN})

        set(RES_SOURCE_FILES)
        foreach(lang ${ARGS_DEFAULT_ARGS})
        string(REPLACE ".po" "" rawlang ${lang})
        add_custom_command(
        OUTPUT
            ${CMAKE_CURRENT_BINARY_DIR}/${rawlang}/${CMAKE_PROJECT_NAME}.resources.dll
        COMMAND
            ${MSGFMT_EXECUTABLE}
        ARGS
            "--csharp"
            "-r"
            ${CMAKE_PROJECT_NAME}
            "-l"
            ${rawlang}
            ${CMAKE_CURRENT_SOURCE_DIR}/${lang}
            "-d"
            "${CMAKE_BINARY_DIR}"
            ${ARGS_OPTIONS}
        DEPENDS
            ${CMAKE_CURRENT_SOURCE_DIR}/${lang}
        COMMENT
            "msgfmt ${lang}"
        )
        list(APPEND RES_SOURCE_FILES "${CMAKE_CURRENT_BINARY_DIR}/${rawlang}/${CMAKE_PROJECT_NAME}.resources.dll")
        endforeach(lang ${ARGS_DEFAULT_ARGS})
        add_custom_target("msgfmt" ALL DEPENDS ${RES_SOURCE_FILES})
    else(${MSGFMT_FOUND})
        message(STATUS "Skipping msgfmt locale generation!")
    endif(${MSGFMT_FOUND})
endmacro(gettext)
