find_program(XGETTEXT_EXECUTABLE NAMES xgettext)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(XGETTEXT DEFAULT_MSG XGETTEXT_EXECUTABLE)

mark_as_advanced(XGETTEXT_EXECUTABLE)

if(${XGETTEXT_FOUND})
    set(FS_SOURCE_FILES)
    foreach(fsfile ${SRC_FS})
        string(REPLACE "${CMAKE_SOURCE_DIR}/src/" "" rawfsfile ${fsfile})
        list(APPEND FS_SOURCE_FILES ${rawfsfile})
    endforeach(fsfile ${SRC_FS})

    add_custom_command(
    OUTPUT
        ${CMAKE_CURRENT_BINARY_DIR}/messages.pot
    COMMAND
        ${XGETTEXT_EXECUTABLE}
    ARGS
        "--language=C#"
        "--from-code=UTF-8"
        "--directory=${CMAKE_SOURCE_DIR}/src"
        ${FS_SOURCE_FILES}
        "-o"
        "messages.pot"
    DEPENDS
        ${SRC_FS}
    COMMENT
        "${SRC_FS}"
    )

    add_custom_command(
    OUTPUT
        ${CMAKE_CURRENT_BINARY_DIR}/glade.pot
    COMMAND
        ${XGETTEXT_EXECUTABLE}
    ARGS
        "--language=Glade"
        "--from-code=UTF-8"
        "--directory=${CMAKE_SOURCE_DIR}/data"
        ${CMAKE_PROJECT_NAME}-ui.xml
        "-o"
        "glade.pot"
    DEPENDS
        ${CMAKE_SOURCE_DIR}/data/${CMAKE_PROJECT_NAME}-ui.xml
    COMMENT
        "${CMAKE_SOURCE_DIR}/data/${CMAKE_PROJECT_NAME}-ui.xml"
    )

    add_custom_target("xgettext"
    # COMMAND
    #     ${XGETTEXT_EXECUTABLE}
    #     "--join-existing"
    #     "--directory=${CMAKE_CURRENT_BINARY_DIR}"
    #     "glade.pot"
    #     "messages.pot"
    #     "-o"
    #     "messages.pot"
    DEPENDS
        ${CMAKE_CURRENT_BINARY_DIR}/glade.pot
        ${CMAKE_CURRENT_BINARY_DIR}/messages.pot
    # COMMENT
    #     "Joining glade.pot messages.pot files"
    )
else(${XGETTEXT_FOUND})
    message(STATUS "Skipping xgettext po/pot generation!")
endif(${XGETTEXT_FOUND})
