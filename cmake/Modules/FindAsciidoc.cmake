# This macro invokes asciidoc and generates the man pages. The following options
# are valid:
#
# SOURCE:  a list of txt source files
# OPTIONS: additional asciidoc options

include(ParseArguments)
find_program(A2X_EXECUTABLE NAMES a2x a2x.py)
find_program(GZIP_EXECUTABLE NAMES gzip)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(A2X DEFAULT_MSG A2X_EXECUTABLE)
find_package_handle_standard_args(GZIP DEFAULT_MSG GZIP_EXECUTABLE)

mark_as_advanced(A2X_EXECUTABLE)
mark_as_advanced(GZIP_EXECUTABLE)

macro(groff output)
    if(${A2X_FOUND})
        parse_arguments(ARGS "SOURCE;OPTIONS" "" ${ARGN})

        set(MAN_SOURCE_FILES)
        foreach(src ${ARGS_DEFAULT_ARGS})
        string(REPLACE ".txt" "" rawsrc ${src})
        list(APPEND MAN_SOURCE_FILES ${rawsrc})
        add_custom_command(
        OUTPUT
            ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}
        COMMAND
            ${A2X_EXECUTABLE}
        ARGS
            "--destination-dir=${CMAKE_CURRENT_BINARY_DIR}"
            "--doctype=manpage"
            "--format=manpage"
            "--no-xmllint"
            ${ARGS_OPTIONS}
            ${CMAKE_CURRENT_SOURCE_DIR}/${rawsrc}.txt
        DEPENDS
            ${CMAKE_CURRENT_SOURCE_DIR}/${rawsrc}.txt
        COMMENT
            "${src}"
        )
        list(APPEND MAN_FILES ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc})
        endforeach(src ${ARGS_DEFAULT_ARGS})
        add_custom_target("asciidoc" ALL DEPENDS ${MAN_FILES})

        set(MAN_GZ_FILES)
        foreach(src ${ARGS_DEFAULT_ARGS})
        string(REPLACE ".txt" "" rawsrc ${src})
        add_custom_command(
        OUTPUT
            ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}.gz
        COMMAND
            ${GZIP_EXECUTABLE}
        ARGS
            "--best"
            "-c"
            ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}
            ">"
            ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}.gz
        DEPENDS
            ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}
        COMMENT
            "${rawsrc}.gz"
        )
        list(APPEND MAN_GZ_FILES ${CMAKE_CURRENT_BINARY_DIR}/${rawsrc}.gz)
        endforeach(src ${ARGS_DEFAULT_ARGS})
        add_custom_target("gzip" ALL DEPENDS ${MAN_GZ_FILES})
    else(${A2X_FOUND})
        message(STATUS "Skipping manual page generation!")
    endif(${A2X_FOUND})
endmacro(groff)
