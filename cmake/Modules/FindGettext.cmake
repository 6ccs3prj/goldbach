# - Try to find Gettext
# Once done, this will define
#
#  Gettext_FOUND        - system has Gettext
#  Gettext_INCLUDE_DIRS - the Gettext include directories
#  Gettext_LIBRARIES    - link these to use Gettext

include(LibFindMacros)

# GAC regex
file(GLOB Gettext_GAC_LIBRARY "/usr/lib/mono/gac/GNU.Gettext/*")

# Include dir
find_path(Gettext_INCLUDE_DIR
  NAMES GNU.Gettext.dll
  PATHS /usr/lib
        /usr/lib/gettext
        /usr/lib/mono/1.0
        /usr/lib/mono/2.0
        /usr/lib/mono/3.5
        /usr/lib/mono/4.0
        ${Gettext_GAC_LIBRARY}
        /usr/lib/fsharp/v2.0
        /usr/lib/fsharp/v4.0
)

# Finally the library itself
find_file(Gettext_LIBRARY
  NAMES GNU.Gettext.dll
  PATHS /usr/lib
        /usr/lib/gettext
        /usr/lib/mono/1.0
        /usr/lib/mono/2.0
        /usr/lib/mono/3.5
        /usr/lib/mono/4.0
        ${Gettext_GAC_LIBRARY}
        /usr/lib/fsharp/v2.0
        /usr/lib/fsharp/v4.0
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(Gettext_PROCESS_INCLUDES Gettext_INCLUDE_DIR)
set(Gettext_PROCESS_LIBS Gettext_LIBRARY)
libfind_process(Gettext)
