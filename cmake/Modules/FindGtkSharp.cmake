# - Try to find GtkSharp
# Once done, this will define
#
#  GtkSharp_FOUND        - system has GtkSharp
#  GtkSharp_INCLUDE_DIRS - the GtkSharp include directories
#  GtkSharp_LIBRARIES    - link these to use GtkSharp

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(GTK_PKGCONF gtk-sharp-3.0)
libfind_pkg_check_modules(GLIB_PKGCONF glib-sharp-3.0)

list(APPEND sharp "atk-sharp" "cairo-sharp" "gdk-sharp" "gio-sharp" "glib-sharp" "gtk-dotnet" "gtk-sharp" "pango-sharp")

# GAC regex
foreach(gtk ${sharp})
    file(GLOB temp_GAC_LIBRARY "/usr/lib/mono/gac/${gtk}/*")
    # list(APPEND GtkSharp_GAC_LIBRARY ${temp_GAC_LIBRARY})

    # Include dir
    find_path(${gtk}_INCLUDE_DIR
      NAMES ${gtk}.dll
      PATHS ${GTK_PKGCONF_INCLUDE_DIRS}
            ${GLIB_PKGCONF_INCLUDE_DIRS}
            /usr/lib
            /usr/lib/cli/${gtk}-3.0
            /usr/lib/mono/1.0
            /usr/lib/mono/2.0
            /usr/lib/mono/3.5
            /usr/lib/mono/4.0
            ${temp_GAC_LIBRARY}
            /usr/lib/fsharp/v2.0
            /usr/lib/fsharp/v4.0
    )

    # Finally the library itself
    find_file(${gtk}_LIBRARY
      NAMES ${gtk}.dll
      PATHS ${GTK_PKGCONF_LIBRARY_DIRS}
            ${GTK_LIBDIR}
            ${GLIB_PKGCONF_LIBRARY_DIRS}
            ${GLIB_LIBDIR}
            /usr/lib
            /usr/lib/cli/${gtk}-3.0
            /usr/lib/mono/1.0
            /usr/lib/mono/2.0
            /usr/lib/mono/3.5
            /usr/lib/mono/4.0
            ${temp_GAC_LIBRARY}
            /usr/lib/fsharp/v2.0
            /usr/lib/fsharp/v4.0
    )
list(APPEND GtkSharp_INCLUDE_DIR ${gtk}_INCLUDE_DIR)
list(APPEND GtkSharp_LIBRARY ${gtk}_LIBRARY)
endforeach(gtk ${sharp})

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(GtkSharp_PROCESS_INCLUDES GtkSharp_INCLUDE_DIR)
set(GtkSharp_PROCESS_LIBS GtkSharp_LIBRARY)
libfind_process(GtkSharp)
