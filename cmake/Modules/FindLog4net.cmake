# - Try to find Log4net
# Once done, this will define
#
#  Log4net_FOUND        - system has Log4net
#  Log4net_INCLUDE_DIRS - the Log4net include directories
#  Log4net_LIBRARIES    - link these to use Log4net

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(Log4net_PKGCONF log4net)

# GAC regex
file(GLOB Log4net_GAC_LIBRARY "/usr/lib/mono/gac/log4net/*")

# Include dir
find_path(Log4net_INCLUDE_DIR
  NAMES log4net.dll
  PATHS ${Log4net_PKGCONF_INCLUDE_DIRS}
        /usr/lib
        /usr/lib/cli
        /usr/lib/cli/log4net-1.2
        /usr/lib/mono/1.0
        /usr/lib/mono/2.0
        /usr/lib/mono/3.5
        /usr/lib/mono/4.0
        ${Log4net_GAC_LIBRARY}
        /usr/lib/fsharp/v2.0
        /usr/lib/fsharp/v4.0
)

# Finally the library itself
find_file(Log4net_LIBRARY
  NAMES log4net.dll
  PATHS ${Log4net_PKGCONF_LIBRARY_DIRS}
        ${Log4net_LIBDIR}
        /usr/lib
        /usr/lib/cli
        /usr/lib/cli/log4net-1.2
        /usr/lib/mono/1.0
        /usr/lib/mono/2.0
        /usr/lib/mono/3.5
        /usr/lib/mono/4.0
        ${Log4net_GAC_LIBRARY}
        /usr/lib/fsharp/v2.0
        /usr/lib/fsharp/v4.0
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(Log4net_PROCESS_INCLUDES Log4net_INCLUDE_DIR)
set(Log4net_PROCESS_LIBS Log4net_LIBRARY)
libfind_process(Log4net)
