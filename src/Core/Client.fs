// Client.fs

namespace Goldbach.Core

    open Goldbach.Global
    open Goldbach.Typeset

    open GNU.Gettext
    open log4net
    open log4net.Config

    open System
    open System.IO
    open System.Reflection
    open System.Runtime.InteropServices

    module Client =


        /// initialising log4net
        let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
        /// initialising gettext
        let CatalogName = GetInfo.ModuleName
        let Catalog = new GettextResourceManager(CatalogName)

        type CommandLine() =

            do

                Console.ForegroundColor <- GetInfo.color_info
                Logger.Info(String.Format(Catalog.GetString("Starting CLI interface")))
                Console.ResetColor()

                Console.WriteLine(String.Format(Catalog.GetString("Goldbach CLI")))

                /// input file option needed?
                match !GetOptions.input with
                | "" | null -> ()
                | _ -> GetInfo.infile := !GetOptions.input

                /// CLI typeset of MathML files
                match !GetInfo.infile with
                | "" | null -> Console.ForegroundColor <- GetInfo.color_error
                               Logger.Error(String.Format(Catalog.GetString("No input file given!")))
                               Console.ResetColor()
                | _ -> if File.Exists(!GetInfo.infile) then
                           /// ConTeXt backend call
                           let typesetcli = new Typeset.Context(!GetInfo.infile)
                           ()
                       else
                           Console.ForegroundColor <- GetInfo.color_error
                           Logger.Error(String.Format(Catalog.GetString("File {0} not found!"), !GetInfo.infile))
                           Console.ResetColor()

                Console.ForegroundColor <- GetInfo.color_info
                Logger.Info(String.Format(Catalog.GetString("Stopping CLI interface")))
                Console.ResetColor()
