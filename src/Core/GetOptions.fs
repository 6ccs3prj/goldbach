// GetOptions.fs

namespace Goldbach.Core

    open Goldbach.Global
    open Goldbach.Typeset

    open GNU.Gettext
    open log4net
    open log4net.Config

    open Microsoft.FSharp.Text

    open System
    open System.Reflection
    open System.Runtime.InteropServices

    module GetOptions =


        /// initialising log4net
        let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
        /// initialising gettext
        let CatalogName = GetInfo.ModuleName
        let Catalog = new GettextResourceManager(CatalogName)

        let verbose = ref false
        let nw = ref false
        let input = ref ""
        let output = ref ""

        /// F# getopts
        let Usage =
            [ArgInfo("--debug", ArgType.Set verbose, String.Format(Catalog.GetString("outputs debug information")));
             ArgInfo("--input", ArgType.String (fun s -> input := s), String.Format(Catalog.GetString("input file")));
             ArgInfo("--output", ArgType.String (fun s -> output := s), String.Format(Catalog.GetString("output file")));
             ArgInfo("--no-window-system", ArgType.Set nw, String.Format(Catalog.GetString("disables the graphical interface")));
             ArgInfo("--version", ArgType.Unit (fun _ ->
                                                    Console.WriteLine(String.Format(Catalog.GetString("Goldbach {0}"), GetInfo.Version))
                                                    Console.WriteLine(String.Format("Copyright © {0} {1}, {2}", GetInfo.Year, GetInfo.Author, GetInfo.Company))
                                                    // write GNU lines?
                                                    Environment.Exit(0)), String.Format(Catalog.GetString("output version information and exit")));
            ]
