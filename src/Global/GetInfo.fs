// GetInfo.fs

namespace Goldbach.Global

    open GNU.Gettext
    open log4net
    open log4net.Config

    open Microsoft.FSharp.Text

    open System
    open System.Reflection
    open System.Runtime.InteropServices

    module GetInfo =


        /// global program info
        let Author = "Adam Reviczky"
        let Company = "King's College London"
        let License = "GPLv3"
        let ModuleName = "goldbach"
        let mutable Version = "1.0"
        let mutable Year = "2010-2011"

        /// log4net colors
        let color_info = ConsoleColor.Blue
        let color_debug = ConsoleColor.DarkGreen
        let color_error = ConsoleColor.Red

        /// CLI input filename
        let infile = ref ""
