// Global.fs

namespace Goldbach.Global

    open GNU.Gettext
    open log4net
    open log4net.Config

    open System
    open System.Reflection
    open System.Runtime.InteropServices

    module Global =


        type Initialise(event : string, signal : string) =

            do

                /// initialising log4net
                let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
                /// initialising gettext
                let CatalogName = GetInfo.ModuleName
                let Catalog = new GettextResourceManager(CatalogName)

                Console.ForegroundColor <- GetInfo.color_debug
                Logger.Debug(String.Format(Catalog.GetString(event + " {0} widget"), signal))
                Console.ResetColor()
