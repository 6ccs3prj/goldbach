// AssemblyInfo.fs

namespace Goldbach.Gui

    open System
    open System.Reflection

    /// assembly information
    [<assembly:AssemblyVersionAttribute("1.0.0.0")>]
    [<assembly:AssemblyCultureAttribute("en")>]
    [<assembly:AssemblyTitleAttribute("Goldbach")>]
    [<assembly:AssemblyDescriptionAttribute("BSc Final year project")>]
    [<assembly:AssemblyCopyrightAttribute("2010-2011 © Adam Reviczky")>]
    [<assembly:log4net.Config.XmlConfigurator(ConfigFile = "Log4net.xml", Watch = true)>]
    do()
