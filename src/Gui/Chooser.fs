// Chooser.fs

namespace Goldbach.Gui

    open Goldbach.Global
    open Goldbach.Typeset
    open Goldbach.Core

    open GNU.Gettext
    open log4net
    open log4net.Config

    open Gtk
    open Gdk

    open System
    open System.IO
    open System.Reflection

    type Chooser(parent, action) as this =
        [<DefaultValue>] val mutable dialog : FileChooserDialog
        [<DefaultValue>] val mutable filename : string
        [<DefaultValue>] val mutable extension : string

        do

            /// initialising log4net
            let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
            /// initialising gettext
            let CatalogName = GetInfo.ModuleName
            let Catalog = new GettextResourceManager(CatalogName)

            Global.Initialise("Opening", "Gtk.FileChooserDialog")

            let mutable choosertitle = String.Format(Catalog.GetString("Choose the file to open"))
            let mutable chooserdefault = FileChooserAction.Open
            let mutable chooseraction = "Open"
            let mutable chooseractionstock = Gtk.Stock.Open

            /// FileChooser Open or Save
            match action with
            | "open" -> choosertitle <- String.Format(Catalog.GetString("Choose the file to open"))
                        chooserdefault <- FileChooserAction.Open
                        chooseraction <- "Open"
                        chooseractionstock <- Gtk.Stock.Open
                        ()
            | "save" -> choosertitle <- String.Format(Catalog.GetString("Choose the file to restore to"))
                        chooserdefault <- FileChooserAction.Save
                        chooseraction <- "Save"
                        chooseractionstock <- Gtk.Stock.Save
                        ()

            /// `menu_proxy_module_load':
            /// mono:
            /// undefined symbol: menu_proxy_module_load
            /// Gtk-WARNING **:
            /// Failed to load type module:
            /// (null)
            this.dialog <- new Gtk.FileChooserDialog(choosertitle,
                                                     parent,
                                                     chooserdefault,
                                                     Gtk.Stock.Cancel,
                                                     ResponseType.Cancel,
                                                     chooseractionstock,
                                                     ResponseType.Accept)

            /// local files only
            // dialog.LocalOnly <- true

            /// XML filter
            let mutable filter_xml = new Gtk.FileFilter()
            filter_xml.Name <- String.Format(Catalog.GetString("{0} files"), "XML")
            filter_xml.AddMimeType("text/xml")
            filter_xml.AddPattern("*.xml")
            this.dialog.AddFilter(filter_xml)

            /// MathML filter
            let mutable filter_mathml = new Gtk.FileFilter()
            filter_mathml.Name <- String.Format(Catalog.GetString("{0} files"), "MathML")
            filter_mathml.AddMimeType("text/xml")
            filter_mathml.AddPattern("*.xml")
            filter_mathml.AddPattern("*.mathml")
            this.dialog.AddFilter(filter_mathml)

            /// TeX filter
            let mutable filter_tex = new Gtk.FileFilter()
            filter_tex.Name <- String.Format(Catalog.GetString("{0} files"), "TeX")
            filter_tex.AddMimeType("text/x-tex")
            filter_tex.AddPattern("*.tex")
            filter_tex.AddPattern("*.mkii")
            filter_tex.AddPattern("*.mkiv")
            filter_tex.AddPattern("*.mkvi")
            filter_tex.AddPattern("*.lua")
            this.dialog.AddFilter(filter_tex)

            /// PNG filter
            let mutable filter_png = new Gtk.FileFilter()
            filter_png.Name <- String.Format(Catalog.GetString("{0} files"), "PNG")
            filter_png.AddMimeType("image/png")
            filter_png.AddPattern("*.png")
            if action = "save" then
                this.dialog.AddFilter(filter_png)

            /// MP filter
            let mutable filter_mp = new Gtk.FileFilter()
            filter_mp.Name <- String.Format(Catalog.GetString("{0} files"), "MetaPost")
            filter_mp.AddMimeType("text/x-metapost")
            filter_mp.AddPattern("*.mp")
            if action = "save" then
                this.dialog.AddFilter(filter_mp)

            /// SVG filter
            let mutable filter_svg = new Gtk.FileFilter()
            filter_svg.Name <- String.Format(Catalog.GetString("{0} files"), "SVG")
            filter_svg.AddMimeType("image/svg+xml")
            filter_svg.AddPattern("*.svg")
            filter_svg.AddPattern("*.svgz")
            if action = "save" then
                this.dialog.AddFilter(filter_svg)

            /// PDF filter
            let mutable filter_pdf = new Gtk.FileFilter()
            filter_pdf.Name <- String.Format(Catalog.GetString("{0} files"), "PDF")
            filter_pdf.AddMimeType("application/pdf")
            filter_pdf.AddPattern("*.pdf")
            if action = "save" then
                this.dialog.AddFilter(filter_pdf)

            /// REST
            let mutable filter_all = new Gtk.FileFilter()
            filter_all.Name <- String.Format(Catalog.GetString("All files"))
            filter_all.AddPattern("*")
            this.dialog.AddFilter(filter_all)

            try
                try
                    /// ResponseType.Accept      = -3
                    /// ResponseType.DeleteEvent = -4
                    /// ResponseType.Cancel      = -6

                    let dialogInit = this.dialog.Run()

                    match dialogInit with
                    | -3 -> this.filename <- this.dialog.Filename
                            this.extension <- this.dialog.Filter.Name

                    /// no ResponseType check
                    // dialog.Run()
                    this.dialog.Destroy()
                with
                    | exn ->
                        Console.ForegroundColor <- GetInfo.color_error
                        Logger.Error(String.Format(Catalog.GetString("Exception occured: {0}"), "Gtk.FileChooserDialog"))
                        Console.ResetColor()
                        // reraise()
            finally
                Console.ForegroundColor <- GetInfo.color_debug
                Logger.Debug(String.Format(Catalog.GetString("{0} section of {1}"), "finally", "Gtk.FileChooserDialog"))
                Console.ResetColor()

            Global.Initialise("Closing", "Gtk.FileChooserDialog")
            ()

        member this.path = this.filename

        member this.ext = this.extension
