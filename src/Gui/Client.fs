// Client.fs

namespace Goldbach.Gui

    open Goldbach.Global
    open Goldbach.Typeset
    open Goldbach.Core

    open GNU.Gettext
    open log4net
    open log4net.Config

    open Gtk
    open Gdk

    // open Microsoft.FSharp.Control

    open System
    open System.Diagnostics
    open System.IO
    open System.Reflection
    open System.Runtime.InteropServices
    open System.Text

    module Gtk =


        let mutable Title = "Goldbach"
        let Glade = "goldbach-ui.xml"
        let GladeWindow = "main_window"

        /// initialising log4net
        let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
        /// initialising gettext
        let CatalogName = GetInfo.ModuleName
        let Catalog = new GettextResourceManager(CatalogName)

        type Initialise() =

            do

                Console.ForegroundColor <- GetInfo.color_info
                Logger.Info(String.Format(Catalog.GetString("Starting Gtk+ interface")))
                Console.ResetColor()

        type GtkWindow =
            inherit Gtk.Window
            [<Builder.Object>] [<DefaultValue>] val mutable main_toolbutton_open : ToolButton
            [<Builder.Object>] [<DefaultValue>] val mutable main_toolbutton_saveas : ToolButton
            [<Builder.Object>] [<DefaultValue>] val mutable main_toolbutton_convert : ToolButton
            [<Builder.Object>] [<DefaultValue>] val mutable main_toolbutton_about : ToolButton
            [<Builder.Object>] [<DefaultValue>] val mutable typeset_viewport : Viewport
            [<Builder.Object>] [<DefaultValue>] val mutable typeset_image : Image
            [<Builder.Object>] [<DefaultValue>] val mutable source_label : Label
            [<Builder.Object>] [<DefaultValue>] val mutable source_textview : TextView
            [<Builder.Object>] [<DefaultValue>] val mutable ocr_label : Label
            [<Builder.Object>] [<DefaultValue>] val mutable ocr_frame : Frame
            [<Builder.Object>] [<DefaultValue>] val mutable ocr_button_clear : Button
            [<Builder.Object>] [<DefaultValue>] val mutable ocr_button_spinner : Button
            [<Builder.Object>] [<DefaultValue>] val mutable ocr_spinner : Spinner
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_relational : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_ellipses : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_operator : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_arrow : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_logical : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_sets : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_misc : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_greeklow : ToolItemGroup
            [<Builder.Object>] [<DefaultValue>] val mutable palette_toolitemgroup_greekup : ToolItemGroup
            [<DefaultValue>] val mutable typesetme : Typeset.Context
            [<DefaultValue>] val mutable SymbolRecognition : Scribble
            [<DefaultValue>] val mutable changestate : bool
            [<DefaultValue>] val mutable detexifyd : Label

            new () as this =
                new GtkWindow(new Builder(Glade), GladeWindow)

                then
                    this.IconName <- ""
                    try
                        this.Icon <- Gdk.Pixbuf(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "resource/goldbach.svg"))
                    with
                        | :? GLib.GException ->
                            Console.ForegroundColor <- GetInfo.color_error
                            Logger.Error(String.Format(Catalog.GetString("Exception occured: {0}"), "GLib.GException"))
                            Console.ResetColor()

                    this.main_toolbutton_open.Label <- String.Format(Catalog.GetString("Open"))
                    this.main_toolbutton_saveas.Label <- String.Format(Catalog.GetString("Save As"))
                    this.main_toolbutton_convert.Label <- String.Format(Catalog.GetString("Generate"))
                    this.main_toolbutton_about.Label <- String.Format(Catalog.GetString("About"))
                    this.source_label.Text <- String.Format(Catalog.GetString("Untitled Document {0}"), "1")
                    this.ocr_label.Text <- String.Format(Catalog.GetString("Symbol recognition"))
                    this.palette_toolitemgroup_relational.Label <- String.Format(Catalog.GetString("Relational symbols"))
                    this.palette_toolitemgroup_ellipses.Label <- String.Format(Catalog.GetString("Ellipses"))
                    this.palette_toolitemgroup_operator.Label <- String.Format(Catalog.GetString("Operator symbols"))
                    this.palette_toolitemgroup_arrow.Label <- String.Format(Catalog.GetString("Arrow symbols"))
                    this.palette_toolitemgroup_logical.Label <- String.Format(Catalog.GetString("Logical symbols"))
                    this.palette_toolitemgroup_sets.Label <- String.Format(Catalog.GetString("Set theory symbols"))
                    this.palette_toolitemgroup_misc.Label <- String.Format(Catalog.GetString("Miscellaneous symbols"))
                    this.palette_toolitemgroup_greeklow.Label <- String.Format(Catalog.GetString("Greek lowercase"))
                    this.palette_toolitemgroup_greekup.Label <- String.Format(Catalog.GetString("Greek uppercase"))
                    this.main_toolbutton_saveas.Sensitive <- false
                    this.main_toolbutton_convert.Sensitive <- false

                    this.changestate <- false
                    this.source_textview.Buffer.Changed.Add(fun _ ->
                                                                if not this.changestate then
                                                                    let sb = new StringBuilder(this.source_label.Text)
                                                                    sb.Insert(0, "*")
                                                                    this.source_label.Text <- sb.ToString()
                                                                    this.main_toolbutton_saveas.Sensitive <- true
                                                                    this.main_toolbutton_convert.Sensitive <- true

                                                                    Console.ForegroundColor <- GetInfo.color_debug
                                                                    Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "Changed", "Gtk.TextView.Buffer"))
                                                                    Console.ResetColor()
                                                                    this.changestate <- true)
                    this.Resize(700,585)
                    this.SymbolRecognition <- new Scribble(200, 150)

                    // this.SymbolRecognition.Publish(this.ocr_spinner, this.ocr_button_spinner)
                    // this.ocr_button_spinner.Remove(this.ocr_spinner)
                    // this.ocr_button_spinner.Add(new Gtk.Image(Stock.Quit, IconSize.Menu))

                    let buttonrem = ref "spinner"
                    this.detexifyd <- new Gtk.Label("")
                    // this.detexifyd.Text <- "new"

                    /// workaround for object initialisation
                    this.RemoveButtonSpinner
                    this.ocr_button_spinner.Sensitive <- false
                    buttonrem := "detexifyd"

                    // this.SymbolRecognition.Publish
                    this.SymbolRecognition.Publish.Add(fun x ->
                                                           if x then
                                                               // if !buttonrem = "spinner" then
                                                               //    this.ocr_spinner.Start()
                                                               // elif !buttonrem = "detexifyd" then
                                                               if !buttonrem = "detexifyd" then
                                                                   this.RemoveButtonLabel
                                                                   buttonrem := "spinner"
                                                           else
                                                               if !buttonrem = "spinner" then
                                                                   this.RemoveButtonSpinner
                                                                   buttonrem := "detexifyd"
                                                               )

                    this.ocr_frame.Add(this.SymbolRecognition)

                    /// FIXME: Gtksourceview not supported on Windows!
                    // let manager = new SourceLanguagesManager()
                    // let language = manager.GetLanguageFromMimeType("text/xml")
                    // let buffer = new SourceBuffer(language)
                    // buffer.Highlight <- true
                    // source_textview <- new SourceView(buffer)

                    /// FIXME: integrate gNumerator here

            new (builder, window_name) as this =
                { inherit Gtk.Window(builder.GetRawObject(window_name)) }

                then
                    builder.Autoconnect(this)
                    Console.ForegroundColor <- GetInfo.color_debug
                    Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "Autoconnect", "Gtk.Builder"))
                    Console.ResetColor()

            member this.on_toolbuttonmisc20_clicked(o : System.Object, e : System.EventArgs) =
                /// FIXME: integrate detexify!
                // let ocrd = "∑"
                // let ocrd_mathml = "<sum/>"

                let toolbuttonmisc20_mathml = "<sum/>"
                this.source_textview.Buffer.DeleteSelection(true, true)
                this.source_textview.Buffer.InsertAtCursor(toolbuttonmisc20_mathml + "\n")
                ()

            member this.on_toolbuttongreeklow1_clicked(o : System.Object, e : System.EventArgs) =
                let toolbuttongreeklow1_mathml = "<mi>&alpha;</mi>"
                this.source_textview.Buffer.DeleteSelection(true, true)
                this.source_textview.Buffer.InsertAtCursor(toolbuttongreeklow1_mathml + "\n")
                ()

            member this.on_toolbuttongreeklow2_clicked(o : System.Object, e : System.EventArgs) =
                let toolbuttongreeklow2_mathml = "<mi>&beta;</mi>"
                this.source_textview.Buffer.DeleteSelection(true, true)
                this.source_textview.Buffer.InsertAtCursor(toolbuttongreeklow2_mathml + "\n")
                ()

            // old handler!
            member this.on_toolbutton4_clicked(o : System.Object, e : System.EventArgs) =
                let toolbutton4_mathml = "<plus/>"
                this.source_textview.Buffer.InsertAtCursor(toolbutton4_mathml + "\n")
                ()

            // old handler!
            member this.on_toolbutton5_clicked(o : System.Object, e : System.EventArgs) =
                let toolbutton5_mathml = "<minus/>"
                this.source_textview.Buffer.InsertAtCursor(toolbutton5_mathml + "\n")
                ()

            member this.on_maintoolbuttonopen_clicked(o : System.Object, e : System.EventArgs) =
                if not this.changestate then
                    let OpenBox = new Chooser(this, "open")
                    if not (OpenBox.path = null) then
                        /// clear Buffer!
                        this.source_textview.Buffer.Clear()

                        /// CRLF safe!
                        File.ReadAllLines(OpenBox.path) |> Array.iteri (fun i line ->
                                                                            this.source_textview.Buffer.InsertAtCursor(line + "\n"))
                        this.source_label.Text <- Path.GetFileName(OpenBox.path)

                        Console.ForegroundColor <- GetInfo.color_debug
                        Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "Changed", "Gtk.TextView.Buffer"))
                        Console.ResetColor()
                        this.changestate <- false
                else
                    /// Save changes to document {0} before closing?
                    /// If you don't save, changes from the last 0 seconds will be permanently lost.
                    /// Close without Saving | Cancel | Save As

                    // let md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Warning, ButtonsType.YesNo, "Changes will be lost, continue?")
                    let md = new WarnDiag(this, String.Format(Catalog.GetString("<b>If you don't save, changes will be permanently lost.</b>\n\nContinue without Saving?")))
                    match md.Answer with
                    | "yes" -> let OpenBox = new Chooser(this, "open")
                               if not (OpenBox.path = null) then
                                   /// clear Buffer!
                                   this.source_textview.Buffer.Clear()

                                   /// CRLF safe!
                                   File.ReadAllLines(OpenBox.path) |> Array.iteri (fun i line ->
                                                                                this.source_textview.Buffer.InsertAtCursor(line + "\n"))
                                   this.source_label.Text <- Path.GetFileName(OpenBox.path)

                                   Console.ForegroundColor <- GetInfo.color_debug
                                   Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "Changed", "Gtk.TextView.Buffer"))
                                   Console.ResetColor()
                                   this.changestate <- false
                    | "no" -> ()
                ()

            member this.on_maintoolbuttonsaveas_clicked(o : System.Object, e : System.EventArgs) =
                let SaveBox = new Chooser(this, "save")

                match SaveBox.ext with
                | "XML files" | "MathML files" -> if not (SaveBox.path = null) then
                                                      File.WriteAllLines(SaveBox.path, [| this.source_textview.Buffer.Text |])
                                                      this.source_label.Text <- Path.GetFileName(SaveBox.path)

                                                      Console.ForegroundColor <- GetInfo.color_debug
                                                      Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "Changed", "Gtk.TextView.Buffer"))
                                                      Console.ResetColor()
                                                      this.changestate <- false
                | "SVG files" | "PDF files" -> if (SaveBox.ext = "SVG files") then
                                                   if not (this.typesetme.getfilename = null) then
                                                       File.Copy(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), this.typesetme.getfilename + ".svg"), SaveBox.path, true)
                                                   else
                                                       Console.ForegroundColor <- GetInfo.color_debug
                                                       Logger.Debug(String.Format(Catalog.GetString("Error: {0} not possible without typesetting first"), "Saving"))
                                                       Console.ResetColor()
                                               elif (SaveBox.ext = "PDF files") then
                                                   if not (this.typesetme.getfilename = null) then
                                                       File.Copy(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), this.typesetme.getfilename + ".pdf"), SaveBox.path, true)
                                                   else
                                                       Console.ForegroundColor <- GetInfo.color_debug
                                                       Logger.Debug(String.Format(Catalog.GetString("Error: {0} not possible without typesetting first"), "Saving"))
                                                       Console.ResetColor()
                | _ -> Console.ForegroundColor <- GetInfo.color_debug
                       Logger.Debug(String.Format(Catalog.GetString("Filetype: {0} not supported!"), SaveBox.ext))
                       Console.ResetColor()
                ()

            member this.on_maintoolbuttonconvert_clicked(o : System.Object, e : System.EventArgs) =
                /// GLib async problems in Gtk! UI block (workaround?)
                // async {
                this.typesetme <- new Typeset.Context(this.source_textview, this.typeset_viewport, this.typeset_image)
                //     ()
                // } |> Async.Start
                ()

            member this.on_maintoolbuttonabout_clicked(o : System.Object, e : System.EventArgs) =
                let InfoBox = new About(this)
                ()

            member this.on_ocrbuttonclear_clicked(o : System.Object, e : System.EventArgs) =
                // clear Cairo surface!
                this.SymbolRecognition.ClearSurface()
                this.SymbolRecognition.QueueDraw()
                this.detexifyd.Text <- ""
                this.ocr_button_spinner.Sensitive <- false

            member this.on_ocrbuttonspinner_clicked(o : System.Object, e : System.EventArgs) =
                let ocrd = this.detexifyd.Text

                match ocrd with
                | "∞" -> this.source_textview.Buffer.DeleteSelection(true, true)
                         this.source_textview.Buffer.InsertAtCursor("<infinity/>" + "\n")
                | _ ->   this.source_textview.Buffer.DeleteSelection(true, true)
                         this.source_textview.Buffer.InsertAtCursor("")
                ()

            member this.RemoveButtonSpinner =
                this.ocr_spinner.Stop()
                this.ocr_button_spinner.Remove(this.ocr_spinner)
                this.ocr_button_spinner.Add(this.detexifyd)
                this.ocr_button_spinner.Sensitive <- true
                ()

            member this.RemoveButtonLabel =
                this.ocr_button_spinner.Remove(this.detexifyd)
                this.ocr_button_spinner.Add(this.ocr_spinner)
                this.ocr_button_spinner.Sensitive <- false
                this.ocr_spinner.Start()
                this.detexifyd.Text <- "∞"
                ()

        /// sample xmldoc

        /// <summary>Builds a new string whose characters are the results of applying the function <c>mapping</c>
        /// to each of the characters of the input string and concatenating the resulting
        /// strings.</summary>
        /// <param name="mapping">The function to produce a string from each character of the input string.</param>
        /// <param name="str">The input string.</param>
        /// <returns>The concatenated string.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the input string is null.</exception>
        type GoldbachWindow() =

            do

                Initialise()

                Application.Init()
                let GoldbachWindow = new GtkWindow()
                GoldbachWindow.Title <- String.Format(Catalog.GetString(Title))
                GoldbachWindow.ShowAll()

                GoldbachWindow.DeleteEvent.Add(fun x ->
                                                   if GoldbachWindow.changestate then
                                                       let md = new WarnDiag(GoldbachWindow, String.Format(Catalog.GetString("<b>If you don't save, changes will be permanently lost.</b>\n\nClose without Saving?")))
                                                       match md.Answer with
                                                       | "yes" -> Application.Quit()
                                                                  Console.ForegroundColor <- GetInfo.color_debug
                                                                  Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "DeleteEvent", "Gtk.Window"))
                                                                  Console.ResetColor()

                                                                  Console.ForegroundColor <- GetInfo.color_info
                                                                  Logger.Info(String.Format(Catalog.GetString("Stopping Gtk+ interface")))
                                                                  Console.ResetColor()
                                                       | "no" -> x.RetVal <- true
                                                                 Console.ForegroundColor <- GetInfo.color_debug
                                                                 Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "DeleteEvent", "Gtk.Window"))
                                                                 Console.ResetColor()
                                                   else
                                                       Application.Quit()
                                                       Console.ForegroundColor <- GetInfo.color_debug
                                                       Logger.Debug(String.Format(Catalog.GetString("Received signal: {0} from {1}"), "DeleteEvent", "Gtk.Window"))
                                                       Console.ResetColor()

                                                       Console.ForegroundColor <- GetInfo.color_info
                                                       Logger.Info(String.Format(Catalog.GetString("Stopping Gtk+ interface")))
                                                       Console.ResetColor())

                Application.Run()
