// Recognition.fs

namespace Goldbach.Gui

    open Goldbach.Global
    open Goldbach.Typeset
    open Goldbach.Core

    open Gtk
    open Gdk

    open Microsoft.FSharp.Control

    open System
    open System.Runtime.InteropServices

    type Scribble =
        inherit DrawingArea
        [<DefaultValue>] val mutable surface : Cairo.Surface
        [<DefaultValue>] val mutable evt : Event<bool>
        [<DefaultValue>] val mutable motion : bool
        [<DefaultValue>] val mutable detexifyd : Label

        new (width, height) as this =
            { inherit DrawingArea() }
            then
                // this.motionev <- new Event<unit>()
                this.evt <- new Event<bool>()
                this.motion <- false

                Global.Initialise("Opening", "Cairo.Context")

                this.SetSizeRequest(width, height)
                this.Events <- this.Events ||| EventMask.ButtonPressMask ||| EventMask.PointerMotionMask ||| EventMask.PointerMotionHintMask

        member this.ClearSurface() =
            let ctx = new Cairo.Context(this.surface)
            ctx.SetSourceRGB(1.0, 1.0, 1.0)
            ctx.Paint()
            (ctx :> IDisposable).Dispose()

        member this.DrawBrush(x, y) =
            let ctx = new Cairo.Context(this.surface)
            ctx.Rectangle(x-3.0, y-3.0, 5.0, 5.0)
            ctx.Fill()
            this.QueueDrawArea(int(x)-3, int(y)-3, 5, 5)
            (ctx :> IDisposable).Dispose()

        override this.OnButtonPressEvent(ev) =
            let s =
                if this.surface = null then
                    false
                else
                    let getButtonNumber (myEv) =
                        match myEv with
                        | 1 -> this.DrawBrush(ev.X, ev.Y)
                        | 3 -> this.ClearSurface()
                               this.QueueDraw()
                        | _ -> ()
                    getButtonNumber(Convert.ToInt32(ev.Button))
                    true
            s

        override this.OnConfigureEvent(ev) =
            this.surface <- ev.Window.CreateSimilarSurface(Cairo.Content.Color, this.AllocatedWidth, this.AllocatedHeight)
            this.ClearSurface()
            true

        override this.OnDrawn(ctx) =
            ctx.SetSourceSurface(this.surface, 0, 0)
            ctx.Paint()
            (ctx :> IDisposable).Dispose()
            true

        override this.OnMotionNotifyEvent(ev) =
            let s =
                match this.surface with
                | null -> false
                | _ -> let mutable x = 0
                       let mutable y = 0
                       let mutable state = new Gdk.ModifierType()

                       /// GLib-GObject-CRITICAL **:
                       /// g_object_remove_toggle_ref:
                       /// assertion `G_IS_OBJECT (object)' failed
                       let getPoints = ev.Window.GetPointer(&x, &y, &state)

                       if ((state &&& Gdk.ModifierType.Button1Mask) = Gdk.ModifierType.Button1Mask) then
                           this.DrawBrush(float(x), float(y))
                           /// motion event
                           this.evt.Trigger(true)
                       else
                           /// no motion event
                           this.evt.Trigger(false)

                       true
            s

        member this.Publish = this.evt.Publish

        // member this.ToFile =
            // def get_picture(self, event, data):
            //     drawable = self.movie_window.window
            //     colormap = drawable.get_colormap()
            //     pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, 0, 8, *drawable.get_size())
            //     pixbuf = pixbuf.get_from_drawable(drawable, colormap, 0,0,0,0, *drawable.get_size())
            //     pixbuf.save(r'somefile.png', 'png')
            //     pixbuf.save(r'somefile.jpeg', 'jpeg')

        member this.InnerPublish(spinner : Spinner, button : Button) =
            let buttonrem = ref "spinner"
            this.detexifyd <- new Gtk.Label("+")
            let ocrd = ref this.detexifyd
            // button.Remove(spinner)
            // button.Add(this.detexifyd)

            this.evt.Publish.Add(fun x ->
                                     if x then
                                         async {
                                             spinner.Start()
                                         } |> Async.Start
                                         button.Sensitive <- false

                                         match !buttonrem with
                                         | "spinner" -> spinner.Start()
                                         | "detexifyd" -> button.Sensitive <- false
                                                          spinner.Start()
                                                          buttonrem := "spinner"
                                                          button.Remove(this.detexifyd)
                                                          button.Add(spinner)
                                     else
                                         async {
                                             Async.Sleep(3000)
                                             spinner.Stop()
                                         } |> Async.Start

                                         match !buttonrem with
                                         | "spinner" -> spinner.Stop()
                                                        button.Remove(spinner)
                                                        button.Add(this.detexifyd)
                                                        button.Sensitive <- true
                                                        buttonrem := "detexifyd"
                                     )
            this.evt.Publish
            // this.evt.Publish.Add(fun _ -> printfn "motion!")
            ()
