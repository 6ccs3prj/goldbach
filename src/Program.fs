// Program.fs

module internal Goldbach.Program

open Goldbach.Global
open Goldbach.Typeset
open Goldbach.Core
open Goldbach.Gui

open GNU.Gettext
open log4net
open log4net.Config

open System
open System.Reflection
open System.Runtime.InteropServices

/// initialising log4net
let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
/// initialising gettext
let CatalogName = GetInfo.ModuleName
let Catalog = new GettextResourceManager(CatalogName)
/// assembly version
let Version = Assembly.GetExecutingAssembly().GetName().Version

[<EntryPoint>]
[<STAThread()>]
let main(args : string[]) =

    /// F# getopt command line parser
    let Options = ref None
    let _ =
        try
            ArgParser.Parse(GetOptions.Usage,
                            (fun x ->
                                 match !Options with
                                 | Some _ ->
                                     failwith (String.Format(Catalog.GetString("more than one input given")))
                                 | None ->
                                     Options := Some x
                                     GetInfo.infile := x), String.Format(Catalog.GetString("{0} <filename>"), GetInfo.ModuleName))
        with
            | Failure(msg) ->
                Console.ForegroundColor <- GetInfo.color_error
                Logger.Error(String.Format(Catalog.GetString("Exception occured: {0}"), msg))
                Console.ResetColor()
                // FIXME: dont quit, get first file instead!
                Environment.Exit(1)

    if !GetOptions.verbose then
        LogManager.GetRepository().Threshold <- Core.Level.Debug
        Console.ForegroundColor <- GetInfo.color_info
        Logger.Info(String.Format(Catalog.GetString("Debugging mode: {0}"), !GetOptions.verbose))
        Console.ResetColor()

    Console.ForegroundColor <- GetInfo.color_info
    Logger.Info(String.Format(Catalog.GetString("Starting Goldbach {0}"), Version))
    Logger.Info(String.Format(Catalog.GetString("Initialising i18n catalog: {0}"), CatalogName))
    Console.ResetColor()

    if !GetOptions.nw then
        /// starting CLI interaction
        let GoldbachTerminal = new Client.CommandLine()
        ()
    else
        /// loading Gtk+ GUI
        let GoldbachGtkWindow = new Gtk.GoldbachWindow()
        ()

    0
