// AssemblyInfo.fs

namespace Goldbach.Typeset

    open System
    open System.Reflection

    /// assembly information
    [<assembly:AssemblyVersionAttribute("1.0.0.0")>]
    [<assembly:AssemblyCultureAttribute("en")>]
    [<assembly:log4net.Config.XmlConfigurator(ConfigFile = "Log4net.xml", Watch = true)>]
    do()
