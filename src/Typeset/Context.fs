// Context.fs

namespace Goldbach.Typeset

    open Goldbach.Global

    open Gtk
    open Gdk

    open GNU.Gettext
    open log4net
    open log4net.Config

    open System
    open System.Diagnostics
    open System.IO
    open System.Reflection
    open System.Runtime.InteropServices

    module Typeset =

    /// Unexpected failure while writing HTML docs:
    /// Exception of type 'Microsoft.FSharp.Metadata.Reader.Internal.Tast+InternalUndefinedItemRef' was thrown.
        (*type System.Diagnostics.Process with
            member this.AsyncWaitForExit( ?millisecondsTimeout ) =
                async {
                    use h = new System.Threading.EventWaitHandle( false, System.Threading.EventResetMode.ManualReset )
                    h.SafeWaitHandle <- new Microsoft.Win32.SafeHandles.SafeWaitHandle( this.Handle, true )
                    match millisecondsTimeout with
                    | Some(ms)  -> return! Async.AwaitWaitHandle( h, ms )
                    | None      -> return! Async.AwaitWaitHandle( h )
                    }*)

        type Context(gui : bool, textbuffer : string, viewport : Gtk.Viewport, typesetimage : Gtk.Image) as this =
            [<DefaultValue>] val mutable mathmlfilename : string
            [<DefaultValue>] val mutable myWidth : int
            [<DefaultValue>] val mutable myHeight : int

            do

                /// initialising log4net
                let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
                /// initialising gettext
                let CatalogName = GetInfo.ModuleName
                let Catalog = new GettextResourceManager(CatalogName)

                if gui then
                    this.myWidth <- viewport.Allocation.Width
                    this.myHeight <- viewport.Allocation.Height
                    ()

                Console.ForegroundColor <- GetInfo.color_debug
                Logger.Debug(String.Format(Catalog.GetString("Processing: {0}"), "MathML"))
                Console.ResetColor()
                // printfn "[MathML]\n%s\n[MathML]" textbuffer.Buffer.Text

                Directory.CreateDirectory(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"))

                // working filename
                this.mathmlfilename <- String.Format("mathml_{0}", System.DateTime.Now.Ticks)
                this.markiv(this.mathmlfilename, textbuffer)

                // Mark II
                let backend_legacy = "texexec"
                // Mark IV
                let backend = "context"

                /// default backend
                let backendinuse = backend
                this.contextbackend(backendinuse, this.mathmlfilename)
                this.pdf2svgbackend(this.mathmlfilename)

                /// async problems!
                /// at (wrapper managed-to-native) GLib.ToggleRef.g_object_remove_toggle_ref (intptr,GLib.ToggleRef/ToggleNotifyHandler,intptr) <0x00004>
                if gui then
                    async {
                        typesetimage.FromPixbuf <- Gdk.Pixbuf(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.svg", this.mathmlfilename)), this.myWidth, this.myHeight)
                    } |> Async.StartImmediate

                /// ASYNC START
                let ctx =
                    async {
                        let backendinuse = backend
                        use proca = new System.Diagnostics.Process()
                        proca.StartInfo.WorkingDirectory <- Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output")
                        proca.StartInfo.FileName <- backendinuse
                        proca.StartInfo.Arguments <- String.Format("--once --batchmode --noconsole --silent --purgeall {0}", String.Format("{0}.tex", this.mathmlfilename))
                        proca.StartInfo.CreateNoWindow <- true
                        proca.StartInfo.UseShellExecute <- false
                        proca.StartInfo.RedirectStandardError <- true
                        proca.StartInfo.RedirectStandardInput <- true
                        proca.StartInfo.RedirectStandardOutput <- true
                        proca.Start() |> ignore
                        let output = proca.StandardOutput.ReadLine()
                        // let! a = proca.AsyncWaitForExit()
                        ()
                    }

                let pdfsvg =
                    async {
                        use procb = new System.Diagnostics.Process()
                        procb.StartInfo.FileName <- @"pdf2svg"
                        procb.StartInfo.Arguments <- String.Format("{0} {1} all", Path.Combine("output", String.Format("{0}.pdf", this.mathmlfilename)), Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.svg", this.mathmlfilename)))
                        procb.Start() |> ignore
                        // let! b = procb.AsyncWaitForExit()
                        ()
                    }
                    // } |> Async.Start
                    // } |> Async.StartImmediate

                // let ctxrun() = ctx |> Async.StartImmediate // |> Async.RunSynchronously

                let rendered =
                    async {
                        typesetimage.FromPixbuf <- Gdk.Pixbuf(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.svg", this.mathmlfilename)), this.myWidth, this.myHeight)
                    }

                let doasync() =
                    async {
                        do! ctx
                        do! pdfsvg
                        do! rendered
                    }
                // doasync() |> Async.StartImmediate

                // let proca = Process.Start("context", String.Format("--once --batchmode --noconsole --silent {0}", Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.tex", this.mathmlfilename))))
                // let procb = Process.Start("texexec", String.Format("{0}", String.Format("{0}.tex", this.mathmlfilename)))
                // let procc = Process.Start("pdf2svg", String.Format("{0} {1} all", String.Format("{0}.pdf", this.mathmlfilename), String.Format("{0}.svg", this.mathmlfilename)))
                // let procd = Process.Start("convert", String.Format("-density 600x600 {0} -transparent white -quality 90 -resize 50% {1}", String.Format("{0}.pdf", this.mathmlfilename), String.Format("{0}.png", this.mathmlfilename)))

                // if (Gdk.Pixbuf(String.Format("{0}.png", this.mathmlfilename))).Height > 150 then
                //     typesetimage.FromPixbuf <- Gdk.Pixbuf(String.Format("{0}.png", this.mathmlfilename), myWidth, myHeight)
                // else
                //     typesetimage.FromPixbuf <- Gdk.Pixbuf(String.Format("{0}.png", this.mathmlfilename))
                /// ASYNC STOP

                /// dangerous delete!
                /// DISABLED by default
                // Directory.Delete(path, true)

                Console.ForegroundColor <- GetInfo.color_debug
                Logger.Debug(String.Format(Catalog.GetString("Closing backend: {0}"), "ConTeXt"))
                Console.ResetColor()

            new(filename : string) =
                let mutable contenttxt = File.ReadAllText(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename))
                Context(false, contenttxt, null, null)

            new(textbuffer : Gtk.TextView, viewport : Gtk.Viewport, typesetimage : Gtk.Image) =
                let getbuffer = textbuffer.Buffer.Text
                Context(true, getbuffer, viewport, typesetimage)

            /// Mark II typesetting options
            member this.markii(mathmlfilename) =
                use typesetstream = new StreamWriter(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.tex", mathmlfilename)), false)
                typesetstream.WriteLine("\usemodule[mathml]")
                typesetstream.WriteLine("\startTEXpage")
                typesetstream.WriteLine("\startXMLdata")
                typesetstream.WriteLine("\stopXMLdata")
                typesetstream.WriteLine("\stopTEXpage")
                typesetstream.Close()

            /// Mark IV typesetting options
            member this.markiv(mathmlfilename, buffer) =
                use typesetstream = new StreamWriter(Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.tex", mathmlfilename)), false)
                typesetstream.WriteLine("\usemodule[mathml]")
                typesetstream.WriteLine("\startTEXpage")
                typesetstream.WriteLine("\xmlprocessdata{}{")
                match buffer with
                | "" -> typesetstream.WriteLine("<math xmlns='http://www.w3.org/1998/Math/MathML'>")
                        typesetstream.WriteLine("</math>")
                | _ -> typesetstream.WriteLine(buffer)
                typesetstream.WriteLine("}{}")
                typesetstream.WriteLine("\stopTEXpage")
                typesetstream.Close()

            /// Mark VI typesetting options, defaults to Mark IV for now
            member this.markvi(mathmlfilename) =
                this.markiv(mathmlfilename)

            member this.contextbackend(backendinuse, mathmlfilename) =
                /// initialising log4net
                let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
                /// initialising gettext
                let CatalogName = GetInfo.ModuleName
                let Catalog = new GettextResourceManager(CatalogName)

                try
                    use proca = new Process()
                    proca.StartInfo.WorkingDirectory <- Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output")
                    proca.StartInfo.FileName <- backendinuse
                    proca.StartInfo.Arguments <- String.Format("--once --batchmode --noconsole --silent --purgeall {0}", String.Format("{0}.tex", mathmlfilename))
                    proca.StartInfo.CreateNoWindow <- true
                    proca.StartInfo.UseShellExecute <- false
                    proca.StartInfo.RedirectStandardError <- true
                    proca.StartInfo.RedirectStandardInput <- true
                    proca.StartInfo.RedirectStandardOutput <- true
                    proca.Start() |> ignore
                    let output = proca.StandardOutput.ReadLine()
                    proca.WaitForExit()
                with
                    | exn ->
                        Console.ForegroundColor <- GetInfo.color_error
                        Logger.Error(String.Format(Catalog.GetString("Exception occured: {0}"), "context"))
                        Console.ResetColor()
                        // reraise()
                    // | :? -1
                    /// automatic healing for context:
                    /// mtxrun --generate
                    /// context --make

            member this.pdf2svgbackend(mathmlfilename) =
                /// initialising log4net
                let Logger : ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)
                /// initialising gettext
                let CatalogName = GetInfo.ModuleName
                let Catalog = new GettextResourceManager(CatalogName)

                try
                    /// needs pdf2svg, replace it with cairo later on!
                    let procb = Process.Start("pdf2svg",
                                              String.Format("{0} {1} all", Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.pdf", mathmlfilename)), Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "output"), String.Format("{0}.svg", mathmlfilename))))
                    procb.WaitForExit()
                with
                    | exn ->
                        Console.ForegroundColor <- GetInfo.color_error
                        Logger.Error(String.Format(Catalog.GetString("Exception occured: {0}"), "pdf2svg"))
                        Console.ResetColor()
                        // reraise()

            member this.getfilename = this.mathmlfilename

            /// ConTeXt typesetting options
            member this.contextoptions() =
                let context_math_font = "euler"
                let context_math_font_size = "12pt"
                let context_math_color = "black"
                let context_to_metapost = false
                ()
