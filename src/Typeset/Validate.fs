// Validate.fs

namespace Goldbach.Typeset

    open Goldbach.Global

    open Gtk
    open Gdk

    open GNU.Gettext
    open log4net
    open log4net.Config

    open System
    open System.Diagnostics
    open System.IO
    open System.Reflection
    open System.Runtime.InteropServices

    module Validate =


        type XMLValidator() =

            do

                /// MathML DTD

                ()
